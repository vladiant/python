import pathlib
import re
from setuptools import find_namespace_packages, setup


here = pathlib.Path(__file__).parent.resolve()


def version(path):
    contents = (here/path).read_text(encoding='utf-8')
    pattern = r"^__version__ = ['\"]([^'\"]*)['\"]"
    return re.search(pattern, contents, re.M).group(1)


# @see https://github.com/pypa/sampleproject
# @see https://setuptools.readthedocs.io/en/latest/setuptools.html
# @see https://packaging.python.org/guides/packaging-namespace-packages/#native-namespace-packages  # noqa
setup(
    name='aeon',
    version=version('daverona/aeon/__init__.py'),
    author='da Verona',
    author_email='egkimatwork@gmail.com',
    license='MIT',
    description='Python package template',
    long_description=(here/'README.md').read_text(encoding='utf-8'),
    long_description_content_type='text/markdown',
    url='https://gitlab.com/templates/python',
    project_urls={
        'Documentation': 'https://gitlab.com/templates/python',
        'Source': 'https://gitlab.com/templates/python',
        'Tracker': 'https://gitlab.com/daverona/templates/python/-/issues',
    },
    keywords='aeon, hello, template, world',
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3 :: Only',
        'Operating System :: OS Independent',
    ],
    packages=find_namespace_packages(include=['daverona.*']),
    zip_safe=False,

    python_requires='>=3.6, <4',
    entry_points={
        'console_scripts': [
            'aeon-says=daverona.aeon.hello:main',
        ],
        'gui_scripts': [],
    },
    setup_requires=[
        # specify other packages to be present to run setup script
        'setuptools>=38.6.0',
        'wheel',
    ],
    install_requires=[
        # specify other packages on which this package depends
    ],
    extras_require={
        # specify other packages on which extra features of this package depend
    },
    package_data={
        # specify data files placed in this package 
        'daverona.aeon': ['data.txt'],
    },
    data_files=[
        # specify data files placed out of this package
        # warning: won't work with wheel
        ('aeon-data', ['data/data.txt']),
    ],
)
